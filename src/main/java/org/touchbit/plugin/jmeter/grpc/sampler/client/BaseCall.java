/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.touchbit.plugin.jmeter.grpc.sampler.client;

import io.grpc.stub.AbstractStub;

import java.util.Map;
import java.util.TreeMap;

/**
 * TODO: Description
 * Created by Oleg Shaburov on 27.04.2018
 * shaburov.o.a@gmail.com
 */
public abstract class BaseCall<T extends AbstractStub<T>> implements Call<T> {

    private Map<String, String> requestParameters = new TreeMap<>();
    private Map<String, String> responseParameters = new TreeMap<>();

    private String key;

    protected BaseCall(String key) {
        this.key = key;
    }

    @Override
    public String getKey() {
        return key;
    }

    public Map<String, String> getRequestParameters() {
        return requestParameters;
    }

    public Map<String, String> getResponseParameters() {
        return responseParameters;
    }

    protected void putRequestKeys(String... keys) {
        for (String key : keys) {
            requestParameters.put(key, "");
        }
    }

    protected void putResponseKeys(String... keys) {
        for (String key : keys) {
            responseParameters.put(key, "");
        }
    }

}