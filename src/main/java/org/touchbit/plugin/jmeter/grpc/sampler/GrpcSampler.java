/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.touchbit.plugin.jmeter.grpc.sampler;

import org.touchbit.plugin.jmeter.grpc.sampler.client.GrpcSamplerClient;
import org.apache.jmeter.samplers.AbstractSampler;
import org.apache.jmeter.samplers.Entry;
import org.apache.jmeter.samplers.SampleResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by Oleg Shaburov on 27.04.2018
 * shaburov.o.a@gmail.com
 */
public class GrpcSampler extends AbstractSampler {

    private static final Logger log = LoggerFactory.getLogger(GrpcSampler.class);

    private Class<?> clientClass;
    private transient GrpcSamplerClient client = null;

    @Override
    public SampleResult sample(Entry entry) {
        initClass();
        if (this.client == null) {
            log.debug(" >>>>>> Creating gRPC Client");
//            client = this.createJavaClient();
        }

        SampleResult result = new SampleResult();
//        result = client.runTest();
        result.setResponseCode("200");
        return result;
    }

    private void initClass() {
        String name = getClassname().trim();
        try {
            clientClass = Class.forName(name, false, Thread.currentThread().getContextClassLoader());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    private GrpcSamplerClient createJavaClient() {
        if (this.clientClass == null) {
            throw new RuntimeException(" >>>>>>>>>> ErrorSamplerClient");
        } else {
            Object client;
            try {
                client = this.clientClass.newInstance();
                if (log.isDebugEnabled()) {
                    log.debug(" >>>>>>>> Created: " + this.getClassname() + "@" + Integer.toHexString(client.hashCode()));
                }
            } catch (Exception e) {
                log.error(" >>>>>>> Exception creating: " + this.getClassname(), e);
                throw new RuntimeException(" >>>>>>>>>> ErrorSamplerClient", e);
            }

            return (GrpcSamplerClient) client;
        }
    }

    public String getClassname() {
        return getPropertyAsString("classname");
    }

    public Object clone() {
        GrpcSampler clone = (GrpcSampler) super.clone();
        clone.clientClass = this.clientClass;
        return clone;
    }

}
