/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.touchbit.plugin.jmeter.grpc.sampler;

import org.touchbit.plugin.jmeter.grpc.postprocessor.GrpcPostProcessor;
import org.touchbit.plugin.jmeter.grpc.postprocessor.responses.GrpcResponse;
import org.apache.jmeter.processor.gui.AbstractPostProcessorGui;
import org.apache.jmeter.testelement.TestElement;

import javax.swing.*;
import java.awt.*;
import java.util.List;

/**
 * Created by Oleg Shaburov on 27.04.2018
 * shaburov.o.a@gmail.com
 */
public class GrpcPostProcessorGui<T extends GrpcResponse> extends AbstractPostProcessorGui {

    private JComboBox<String> data;

    private static GrpcPostProcessor processor = new GrpcPostProcessor();

    public GrpcPostProcessorGui() {
        super();
        init();
    }

    public static void main(String[] args) {
        for (GrpcResponse grpcResponse : processor.getClassList()) {
            System.out.println(grpcResponse.getGrpcClass().getCanonicalName());
        }
    }

    private GrpcPostProcessorGui(List<T> responseList) {
        super();
        init();
    }

    public String getStaticLabel() {
        return getLabelResource();
    }

    public String getLabelResource() {
        return "gRPC PostProcessor"; // $NON-NLS-1$
    }

    public void configure(TestElement element) {
        for (GrpcResponse grpcResponse : processor.getClassList()) {
            data.addItem(grpcResponse.getGrpcClass().getCanonicalName());
        }
        super.configure(element);
    }

    public TestElement createTestElement() {
        modifyTestElement(processor);
        return processor;
    }

    public void modifyTestElement(TestElement te) {
        te.clear();
        configureTestElement(te);
        te.setProperty("modifyTestElement", data.getSelectedIndex());
    }

    private void init() {
        setLayout(new BorderLayout(0, 5));
        setBorder(makeBorder());
        add(makeTitlePanel(), BorderLayout.NORTH);

        // Specific setup
        add(createDataPanel(), BorderLayout.WEST);
//        add(createDataPanel(), BorderLayout.CENTER);
    }

    private Component createDataPanel() {
        JLabel label = new JLabel("Classname: ");
        data = new JComboBox<>();
        data.setSelectedItem("");
        data.setName("ClassnameJComboBox");
        label.setLabelFor(data);

        JPanel dataPanel = new JPanel();
//        JPanel dataPanel = new JPanel(new BorderLayout(5, 0));

        dataPanel.add(label, BorderLayout.WEST);
        dataPanel.add(data, BorderLayout.WEST);

        return dataPanel;
    }

    public void clearGui() {
        super.clearGui();
        data.removeAllItems();

    }
}
