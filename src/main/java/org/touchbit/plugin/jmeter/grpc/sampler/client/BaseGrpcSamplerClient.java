/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.touchbit.plugin.jmeter.grpc.sampler.client;

import org.touchbit.plugin.jmeter.grpc.transport.StubCallHeaderInterceptor;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.stub.AbstractStub;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * TODO: Description
 * Created by Oleg Shaburov on 27.04.2018
 * shaburov.o.a@gmail.com
 */
public abstract class BaseGrpcSamplerClient implements GrpcSamplerClient {

    private String host = "https://example.com:12345";

    private final StubCallHeaderInterceptor interceptor = new StubCallHeaderInterceptor();

    private Map<String, ManagedChannel> channels = new ConcurrentHashMap<>();

    private List<Call> calls = new ArrayList<>();

    private Service service;

    protected BaseGrpcSamplerClient(Service service, Call... calls) {
        this.service = service;
        this.calls.addAll(Arrays.asList(calls));
    }

    public Service getService() {
        return service;
    }

    public List<String> getCallsKeys() {
        return calls.stream().map(Call::getKey).collect(Collectors.toList());
    }

    public List<Call> getCalls() {
        return calls;
    }

    public Call getCall(String key) {
        return calls.stream().filter(c -> c.getKey().equals(key)).findFirst().orElseThrow(RuntimeException::new);
    }

    @SuppressWarnings("unchecked")
    protected <T extends AbstractStub<? extends AbstractStub>> T getStub(Function<ManagedChannel, T> method) {
        return (T) method.apply(getChannel(getServiceHost())).withInterceptors(interceptor);
    }

    @Override
    public String getServiceHost() {
        return host;
    }

    @Override
    public void setServiceHost(String host) {
        this.host = host;
    }

    private ManagedChannel getChannel(String target) {
        if (channels.get(target) == null) {
            channels.put(target, ManagedChannelBuilder.forTarget(target).usePlaintext(true).build());
        }
        return channels.get(target);
    }

}
