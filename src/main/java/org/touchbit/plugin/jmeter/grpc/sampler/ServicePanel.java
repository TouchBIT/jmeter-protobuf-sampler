/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.touchbit.plugin.jmeter.grpc.sampler;

import org.touchbit.plugin.jmeter.grpc.sampler.client.Call;
import org.touchbit.plugin.jmeter.grpc.sampler.client.GrpcSamplerClient;
import org.touchbit.plugin.jmeter.grpc.sampler.client.Service;
import io.grpc.stub.AbstractStub;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.jmeter.gui.util.HorizontalPanel;
import org.apache.jmeter.util.JMeterUtils;
import org.apache.jorphan.gui.JLabeledChoice;
import org.apache.jorphan.reflect.ClassFinder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.awt.*;
import java.util.*;
import java.util.List;

/**
 * TODO: Description
 * Created by Oleg Shaburov on 27.04.2018
 * shaburov.o.a@gmail.com
 */
public class ServicePanel extends HorizontalPanel {

    private static final Logger log = LoggerFactory.getLogger(ServicePanel.class);
    private static final Map<String, GrpcSamplerClient> grpcSamplerClientMap = new HashMap<>();
    private static final Map<Service, Map<String, List<Call>>> entry = new HashMap<>();

    private final JLabeledChoice servicesChoice = new JLabeledChoice();
    private final JLabeledChoice callsChoice = new JLabeledChoice();
    private final JLabeledChoice classNameLabeledChoice = new JLabeledChoice();
    private final JLabel         warningLabel = new JLabel("warning");

    private Call<AbstractStub> call;

    ServicePanel() {
        setBorder(BorderFactory.createTitledBorder("gRPC service configuration"));
    }

    protected void createClassNamePanel(GrpcSamplerGuiConfig config) {
        log.info(">>> Create service panel");
        List<String> possibleClasses = new ArrayList<>();
        try {
            possibleClasses = ClassFinder
                    .findClassesThatExtend(JMeterUtils.getSearchPaths(), new Class[]{GrpcSamplerClient.class});
        } catch (Exception e) {
            log.debug(">>> Exception getting interfaces.", e);
        }
        if (grpcSamplerClientMap.isEmpty()) {
            for (String clazz : possibleClasses) {
                try {
                    grpcSamplerClientMap.put(clazz, (GrpcSamplerClient) Class.forName(clazz).newInstance());
                } catch (Exception e) {
                    log.error(">>> Error creating new instance of sampler class: {}", clazz, e);
                }
            }
        }
        grpcSamplerClientMap.forEach((clazz, value) -> {
            Map<String, List<Call>> classMap = new HashMap<>();
            List<Call> calls = value.getCalls();
            classMap.put(clazz, calls);
            if(entry.get(value.getService()) == null) {
                entry.put(value.getService(), classMap);
            } else {
                entry.get(value.getService()).put(clazz, calls);
            }
        });
        List<Service> services = new ArrayList<>(entry.keySet());
        services.forEach(s -> servicesChoice.addValue(s.getName()));
        servicesChoice.setSelectedIndex(0);
        servicesChoice.addChangeListener(config);

        Service service = services.stream()
                .filter(s -> s.getName().equalsIgnoreCase(String.valueOf(servicesChoice.getSelectedItems()[0])))
                .findFirst().get();
        List<String> classList = new ArrayList<>(entry.get(service).keySet());
        classList.forEach(classNameLabeledChoice::addValue);
        classNameLabeledChoice.setSelectedIndex(0);
        classNameLabeledChoice.addChangeListener(config);

        String selectedClass = String.valueOf(classNameLabeledChoice.getSelectedItems()[0]);
        List<Call> callsList = entry.get(service).get(selectedClass);
        callsList.forEach(c -> callsChoice.addValue(c.getKey()));
        callsChoice.setSelectedIndex(0);
        callsChoice.addChangeListener(config);
        for (Call call1 : callsList) {
            if (call1.getKey().equals(callsChoice.getSelectedItems()[0])) {
                call = call1;
            }
        }
        config.checkAndCreateNewInstanceSelectedSamplerClass();
        warningLabel.setForeground(Color.RED);
        Font font = this.warningLabel.getFont();
        warningLabel.setFont(new Font(font.getFontName(), Font.BOLD, (int)((double)font.getSize() * 1.1D)));
        warningLabel.setVisible(false);
        HorizontalPanel panel = new HorizontalPanel();
        panel.add(servicesChoice);
        panel.add(classNameLabeledChoice);
        panel.add(callsChoice);
        add(panel, BorderLayout.WEST);
    }

    public void configureService(GrpcSamplerGuiConfig config) {
//        Service s = Service.valueOf(String.valueOf(servicesChoice.getSelectedItems()[0]));
        Service s = null; // TODO
        List<String> classList = new ArrayList<>(entry.get(s).keySet());
        classNameLabeledChoice.setValues(classList.toArray(ArrayUtils.EMPTY_STRING_ARRAY));
        classNameLabeledChoice.setSelectedIndex(0);
        String selectedClass = String.valueOf(classNameLabeledChoice.getSelectedItems()[0]);
        List<Call> callsList = entry.get(s).get(selectedClass);
        List<String> callsKeyList = new ArrayList<>();
        callsList.forEach(c -> callsKeyList.add(c.getKey()));
        callsChoice.setValues(callsKeyList.toArray(ArrayUtils.EMPTY_STRING_ARRAY));
        callsChoice.setSelectedIndex(0);
        for (Call c : callsList) {
            if (c.getKey().equals(callsChoice.getSelectedItems()[0])) {
                call = c;
            }
        }
        config.updateUI();
    }

    public Call<AbstractStub> getCall() {
        return call;
    }

    public JLabel getWarningLabel() {
        return warningLabel;
    }

    public JLabeledChoice getClassNameLabeledChoice() {
        return classNameLabeledChoice;
    }

    public JLabeledChoice getServicesChoice() {
        return servicesChoice;
    }

    public JLabeledChoice getCallsChoice() {
        return callsChoice;
    }
}
