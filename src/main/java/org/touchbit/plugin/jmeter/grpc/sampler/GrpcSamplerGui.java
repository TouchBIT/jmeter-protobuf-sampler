/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.touchbit.plugin.jmeter.grpc.sampler;

import org.apache.jmeter.testelement.TestElement;
import javax.swing.*;
import java.awt.*;

/**
 * GUI сэмплера осуществления gRPC запросов с обработкой ответа и постпроцессингом.
 *
 * Created by Oleg Shaburov on 27.04.2018
 * shaburov.o.a@gmail.com
 */
@SuppressWarnings("unused")
public class GrpcSamplerGui extends GrpcSamplerGuiConfig {

    public GrpcSamplerGui() {
        super();
        Box box = Box.createVerticalBox();
        box.add(makeTitlePanel());
        createClassNamePanel();
        configureRequestParameters();
        box.add(servicePanel);
        requestPanel.add(requestParametersBlock);
        box.add(requestPanel);
        createPostProcessorPanel();
        box.add(postProcessorPanel);
        add(box, BorderLayout.NORTH);
    }

    /**
     * Метод конфигурирования GUI из полученных jmx настроек
     */
    public void configure(TestElement element) {
        LOG.info(">>> GrpcSamplerGui configuration from jmx");
        GrpcSampler sampler = (GrpcSampler) element;
        checkAndCreateNewInstanceSelectedSamplerClass(sampler.getPropertyAsString("sampler_client_classname"));
        configureResponseParametersBlock();
        super.configure(sampler);
    }

    /**
     * Метод создания нового семплера для осуществления запроса.
     */
    public TestElement createTestElement() {
        GrpcSampler grpcSampler = new GrpcSampler();
        LOG.info(">>> Created new GrpcSampler: {}", grpcSampler);
        modifyTestElement(grpcSampler);
        return grpcSampler;
    }

    /**
     * Метод модификации семплера исходя из настроек конфигурации.
     */
    public void modifyTestElement(TestElement element) {
        LOG.info(">>> Modify GrpcSampler: {}", element);
        GrpcSampler sampler = (GrpcSampler) element;
        super.configureTestElement(sampler);
        sampler.setProperty("sampler_client_classname", getSamplerClientClass().getName());
    }

    /**
     * Очищает GUI при переключении между GUI компонентами
     */
    public void clearGui() {
        LOG.info(">>> Cleaning GrpcSamplerGui");
        super.clearGui();
//        classNameLabeledChoice.setSelectedIndex(0);
//        warningLabel.setVisible(false);
//        requestParametersBlock.removeAll();
//        postProcessorCheckBox.setSelected(false);
//        commonErrorParametersCheckBox.setSelected(false);
        configureResponseParametersBlock();
        LOG.debug(">>> GrpcSamplerGui cleared");
    }

    public String getStaticLabel() {
        return getLabelResource();
    }

    public String getLabelResource() {
        return "gRPC Sampler";
    }
}