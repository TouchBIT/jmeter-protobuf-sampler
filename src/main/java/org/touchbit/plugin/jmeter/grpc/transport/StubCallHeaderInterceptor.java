/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.touchbit.plugin.jmeter.grpc.transport;

import io.grpc.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.StringJoiner;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Перехватчик заголовков (header/trailer)
 *
 * Created by Oleg Shaburov on 27.04.2018
 * shaburov.o.a@gmail.com
 */
public class StubCallHeaderInterceptor implements ClientInterceptor {

    private Logger log = LoggerFactory.getLogger(StubCallHeaderInterceptor.class);

    private final AtomicReference<Metadata> requestHeaders = new AtomicReference<>();

    private final AtomicReference<Metadata> responseHeaders = new AtomicReference<>();

    private final AtomicReference<Metadata> responseTrailers = new AtomicReference<>();

    public StubCallHeaderInterceptor() {
        requestHeaders.set(new Metadata());
        addHeader("trace-request-id", UUID.randomUUID().toString());
    }

    public StubCallHeaderInterceptor(Logger logger) {
        this();
        log = logger;
    }

    private void addHeader(String name, String value) {
        Metadata.Key<String> key = Metadata.Key.of(name, Metadata.ASCII_STRING_MARSHALLER);
        requestHeaders.get().put(key, value);
    }

    @Override
    public <E, P> ClientCall<E, P> interceptCall(MethodDescriptor<E, P> method,
                                                 CallOptions callOptions,
                                                 Channel next) {

        log.info("Method full name: {}", method.getFullMethodName());
        log.debug("Method type: {}", method.getType());
        return new ForwardingClientCall.SimpleForwardingClientCall<E, P>(next.newCall(method, callOptions)) {

            @Override
            public void start(Listener<P> responseListener, Metadata headers) {
                headers.merge(requestHeaders.get());
                requestHeaders.set(headers);
                responseHeaders.set(null);
                responseTrailers.set(null);
                super.start(new ForwardingClientCallListener
                        .SimpleForwardingClientCallListener<P>(responseListener) {

                    @Override
                    public void onHeaders(Metadata headers) {
                        responseHeaders.set(headers);
                        super.onHeaders(headers);
                    }

                    @Override
                    public void onClose(Status status, Metadata trailers) {
                        responseTrailers.set(trailers);
                        super.onClose(status, trailers);
                    }
                }, headers);
            }
        };
    }

    public Metadata getRequestMetadata() {
        return requestHeaders.get();
    }

    public String getRequestStringMetadata() {
        Metadata metadata = getRequestMetadata();
        StringJoiner sj = new StringJoiner("\n");
        metadata.keys().forEach(k -> sj.add("    " + k + ": " +
                metadata.get(Metadata.Key.of(k, Metadata.ASCII_STRING_MARSHALLER))));
        return sj.toString();
    }

    public String getResponseStringMetadata() {
        Metadata metadata = getResponseMetadata();
        StringJoiner sj = new StringJoiner("\n");
        metadata.keys().forEach(k -> sj.add("    " + k + ": " +
                metadata.get(Metadata.Key.of(k, Metadata.ASCII_STRING_MARSHALLER))));
        return sj.toString();
    }

    public Metadata getResponseMetadata() {
        Metadata metadata = new Metadata();
        metadata.merge(responseHeaders.get());
        metadata.merge(responseTrailers.get());
        return metadata;
    }

}