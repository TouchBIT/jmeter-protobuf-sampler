/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.touchbit.plugin.jmeter.grpc.postprocessor.responses;

import com.google.protobuf.GeneratedMessageV3;

import javax.swing.*;

/**
 * TODO: Description
 * Created by Oleg Shaburov on 27.04.2018
 * shaburov.o.a@gmail.com
 */
public abstract class GrpcResponse<T extends GeneratedMessageV3> {

    private Class<T> tClass;

    protected GrpcResponse(Class<T> grpc) {
        tClass = grpc;
    }

    abstract JTable getParametersTable();

    abstract Object[][] getData();

    public Class<T> getGrpcClass() {
        return tClass;
    }
}
