/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.touchbit.plugin.jmeter.grpc.sampler;

import org.touchbit.plugin.jmeter.grpc.sampler.client.GrpcSamplerClient;
import org.apache.jmeter.gui.util.HorizontalPanel;
import org.apache.jmeter.gui.util.VerticalPanel;
import org.apache.jmeter.samplers.gui.AbstractSamplerGui;
import org.apache.jorphan.gui.JLabeledTextField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.List;

/**
 * TODO: Description
 * Created by Oleg Shaburov on 27.04.2018
 * shaburov.o.a@gmail.com
 */
abstract class GrpcSamplerGuiConfig extends AbstractSamplerGui implements ChangeListener, ItemListener {

    protected static final Logger LOG = LoggerFactory.getLogger(GrpcSamplerGuiConfig.class);

    protected JPanel requestPanel = new VerticalPanel();
    protected JPanel requestParametersBlock = new JPanel(new GridBagLayout());
    protected JPanel postProcessorPanel = new VerticalPanel();
    protected JPanel checkBoxPanel = new HorizontalPanel();
    protected JCheckBox postProcessorCheckBox = new JCheckBox("Enable", false);
    protected JPanel responseParametersBlock = new JPanel(new GridBagLayout());

    protected ServicePanel servicePanel = new ServicePanel();

    private GrpcSamplerClient grpcSamplerClient;
    private Class<?> grpcSamplerClientClass;

    protected GrpcSamplerGuiConfig() {
        setLayout(new BorderLayout(0, 5));
        setBorder(makeBorder());
        requestPanel.setBorder(BorderFactory.createTitledBorder("Request configuration"));
    }

    protected void createClassNamePanel() {
        servicePanel.createClassNamePanel(this);
    }

    protected void createPostProcessorPanel() {
        LOG.info(">>> Create PostProcessor panel");
        postProcessorPanel.setBorder(BorderFactory.createTitledBorder("Post processor configuration"));
        checkBoxPanel.add(postProcessorCheckBox);
        postProcessorPanel.add(checkBoxPanel);
        postProcessorPanel.add(responseParametersBlock);
        postProcessorCheckBox.addItemListener(this);
        this.updateUI();
    }

    protected void configureResponseParametersBlock() {
        LOG.info(">>> Configure response parameters block");
        responseParametersBlock.removeAll();
        if (postProcessorCheckBox.isSelected()) {
            GridBagConstraints gbc = new GridBagConstraints();
            initConstraints(gbc);
            servicePanel.getCall().getResponseParameters().forEach((k, v) -> {
                JLabeledTextField textField = new JLabeledTextField(k + "  ");
                textField.setText(v);
                addField(responseParametersBlock, textField, gbc);
                resetContraints(gbc);
                LOG.info(">>>  >>>>>>> {}: {}={}", grpcSamplerClientClass.getSimpleName(), k, v);
            });
        }
        this.updateUI();
    }

    protected void configureRequestParameters() {
        LOG.info(">>> Configure request parameters block");
        GridBagConstraints gbc = new GridBagConstraints();
        initConstraints(gbc);
        JLabeledTextField host = new JLabeledTextField("Host address: ");
        host.setText(grpcSamplerClient.getServiceHost());
        addField(requestParametersBlock, host, gbc);
        resetContraints(gbc);
        servicePanel.getCall().getRequestParameters().forEach((k, v) -> {
            JLabeledTextField textField = new JLabeledTextField(k + "  ");
            textField.setText(v);
            addField(requestParametersBlock, textField, gbc);
            resetContraints(gbc);
            LOG.info(">>>  >>>>>>> {}: {}={}", grpcSamplerClientClass.getSimpleName(), k, v);
        });
        postProcessorCheckBox.setSelected(false);
        configureResponseParametersBlock();
        this.updateUI();
    }

    protected void checkAndCreateNewInstanceSelectedSamplerClass(String className) {
        String selectedClass = null;
        String[] items = servicePanel.getClassNameLabeledChoice().getItems();
        for (int i = 0; i < items.length; i++) {
            if (items[i].equals(className)) {
                servicePanel.getClassNameLabeledChoice().setSelectedIndex(i);
                this.updateUI();
            }
        }
        try {
            selectedClass = servicePanel.getClassNameLabeledChoice().getSelectedItems()[0].toString();
            LOG.debug(">>> Selected sampler class: {}", selectedClass);
            grpcSamplerClientClass = Class.forName(selectedClass);
            LOG.debug(">>> Selected sampler class is successfully initialized");
            grpcSamplerClient = (GrpcSamplerClient) grpcSamplerClientClass.newInstance();
            LOG.debug(">>> Successfully created new instance of selected sampler class");
            servicePanel.getWarningLabel().setVisible(false);
            LOG.debug(">>> Warning label is set to invisible");
        } catch (Exception e) {
            LOG.error(">>> Error creating new instance of selected sampler class: {}", selectedClass, e);
            servicePanel.getWarningLabel().setText(e.getClass().getSimpleName() + ": " + e.getMessage());
            LOG.debug(">>> Warning label text message was created");
            servicePanel.getWarningLabel().setVisible(true);
            LOG.debug(">>> Warning label is set to visible");
            postProcessorPanel.setVisible(false);
            LOG.debug(">>> Post processor panel is set to invisible");
            this.updateUI();
            LOG.debug(">>> UI is updated");
            throw new RuntimeException(e);
        }
    }

    protected void checkAndCreateNewInstanceSelectedSamplerClass() {
        checkAndCreateNewInstanceSelectedSamplerClass(null);
    }

    private void resetContraints(GridBagConstraints gbc) {
        gbc.gridx = 0;
        ++gbc.gridy;
        gbc.weightx = 0.0D;
        gbc.fill = 0;
    }

    private void addField(JPanel panel, JLabeledTextField field, GridBagConstraints gbc) {
        List<JComponent> item = field.getComponentList();
        panel.add(item.get(0), gbc.clone());
        ++gbc.gridx;
        gbc.weightx = 1.0D;
        gbc.fill = 2;
        panel.add(item.get(1), gbc.clone());
    }

    private void initConstraints(GridBagConstraints gbc) {
        gbc.anchor = 18;
        gbc.fill = 0;
        gbc.gridheight = 1;
        gbc.gridwidth = 1;
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 0.0D;
        gbc.weighty = 0.0D;
    }

    @Override
    public void stateChanged(ChangeEvent event) {
        if (event.getSource() == servicePanel.getServicesChoice()) {
            LOG.info(">>> Received event: changing service");
            servicePanel.configureService(this);
        }
        if (event.getSource() == servicePanel.getClassNameLabeledChoice()) {
            LOG.info(">>> Received event: changing class name");
            checkAndCreateNewInstanceSelectedSamplerClass();
            postProcessorPanel.setVisible(true);
            LOG.debug(">>> Post processor block is enabled.");
            requestParametersBlock.removeAll();
            LOG.debug(">>> Request parameters removed.");
            configureRequestParameters();
            postProcessorCheckBox.setSelected(false);
            LOG.debug(">>> Post processor check-box is set to unselected.");
        } else {
            LOG.warn(">>> Unknown event: {}", event.getSource().getClass().getName());
        }
    }

    @Override
    public void itemStateChanged(ItemEvent event) {
        if (event.getSource() == postProcessorCheckBox) {
            LOG.info(">>> Received event: change post processor checkbox value: {}", postProcessorCheckBox.isSelected());
            configureResponseParametersBlock();
        } else {
            LOG.warn(">>> Unknown event: {}", event.getSource().getClass().getName());
        }
    }

    protected Class<?> getSamplerClientClass() {
        return grpcSamplerClientClass;
    }
}
