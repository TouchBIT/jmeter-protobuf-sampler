/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.touchbit.plugin.jmeter.grpc.postprocessor;

import com.google.common.reflect.ClassPath;
import org.touchbit.plugin.jmeter.grpc.sampler.GrpcPostProcessorGui;
import org.touchbit.plugin.jmeter.grpc.postprocessor.responses.GrpcResponse;
import org.apache.jmeter.processor.PostProcessor;
import org.apache.jmeter.testelement.AbstractScopedTestElement;
import org.apache.jmeter.threads.JMeterContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by Oleg Shaburov on 27.04.2018
 * shaburov.o.a@gmail.com
 */
public class GrpcPostProcessor extends AbstractScopedTestElement implements PostProcessor, Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(GrpcPostProcessor.class);

    private final List<GrpcResponse> classList = new ArrayList<>();

    public GrpcPostProcessor() {
        synchronized (classList) {
            if (classList.isEmpty()) {
                try {
                    System.out.println(" 1 ");
                    Set<ClassPath.ClassInfo> set = ClassPath.from(GrpcPostProcessorGui.class.getClassLoader())
                            .getTopLevelClassesRecursive("org.touchbit.plugin.jmeter.grpc.postprocessor.responses");
                    System.out.println(" 2 ");
                    for (ClassPath.ClassInfo classInfo : set) {
                        Class<?> clazz = classInfo.load();
                        if (clazz != null && clazz.getSuperclass().isAssignableFrom(GrpcResponse.class) &&
                                !clazz.getSuperclass().isInstance(GrpcResponse.class)) {
                            classList.add((GrpcResponse) clazz.newInstance());
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
    }

    @Override
    public void process() {
        JMeterContext context = this.getThreadContext();
        context.getPreviousResult();
    }

    public List<GrpcResponse> getClassList() {
        return classList;
    }

}
