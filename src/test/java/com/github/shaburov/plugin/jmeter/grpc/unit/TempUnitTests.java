/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.github.shaburov.plugin.jmeter.grpc.unit;

import com.github.shaburov.plugin.jmeter.grpc.BaseTest;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

/**
 * Created by Oleg Shaburov on 20.05.2018
 * shaburov.o.a@gmail.com
 */
public class TempUnitTests extends BaseTest {

    @Test
    @DisplayName("Temp unit test")
    public void unitTest_20180520185905() {

    }

}
