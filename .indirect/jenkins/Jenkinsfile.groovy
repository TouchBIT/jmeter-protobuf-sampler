
/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

def scmVars

node {
    wrap([$class: 'AnsiColorBuildWrapper', 'colorMapName': 'xterm']) {

        cleanWs()

        stage('Checkout') {
            scmVars = checkout scm
            echo "SCM variables: ${scmVars}"
        }

        stage('Verify') {
            sh "mvn clean verify"
        }

        stage('Analyze') {
            withCredentials([string(credentialsId: 'SONAR_ACCESS_TOKEN', variable: 'SONAR_TOKEN'),
                             string(credentialsId: 'GIT_HUB_ACCESS_TOKEN', variable: 'GIT_HUB_TOKEN')]) {

                String mvnComm = "mvn sonar:sonar -Dsonar.login=${SONAR_TOKEN} -Dsonar.github.oauth=${GIT_HUB_TOKEN} "
                String commonArgs="-Dsonar.analysis.buildNumber=${env.BUILD_ID} " +
                        "-Dsonar.analysis.pipeline=${env.BUILD_ID} " +
                        "-Dsonar.analysis.sha1=${scmVars.GIT_COMMIT} " +
                        "-Dsonar.github.repository=\"${scmVars.GIT_URL}\" "
                if ("master" == scmVars.GIT_BRANCH) {
                    sh "${mvnComm} ${commonArgs}"
                } else {
                    sh "${mvnComm} ${commonArgs} -Dsonar.analysis.mode=preview"
                }
            }
        }
    }

}
